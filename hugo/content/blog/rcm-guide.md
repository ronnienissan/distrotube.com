---
title: "Managing dotfiles with style with rcm"
date: 2020-04-23T21:00:00+03:00
image: images/blog/0020.png
author: Ronnie Nissan
---

### Introduction

rcm is a dotfles (rc files) management tool that consists of four programs, rcup, rcdn, mkrc and lsrc.
It is open source, and is maintained and funded by thoughtbot, inc.

I used to copy all my files to a dotfiles folder in my home directory, then use GNU/stow to make all the symlinks
and it worked fine, until I found rcm and my dotfiles game was on a higher level immediately. Using rcm is just fast and intuitive.
rcm is available for linux, BSD and MacOs, and you can find how to install it on your system [here](https://github.com/thoughtbot/rcm).

### usage

If you are just starting to backup your dotfiles and don't have a default directory for them, then you can start away immediately by running
```
mkrc -v <path to the file or directory you want to backup>
```
this will creat a .dotfiles folder in your home directory, and will mv the disered file or directory to it, then it will symlinks all the files to where they were moved from. For example, if you want to add your .bashrc to the list, you'll do the following
```
mkrc -v ~/.bashrc
```
the output of the above command will be
```shell
Moving...
'/home/$USER/.bashrc' -> '/home/$USER/.dotfiles/.bashrc'
Linking...
'/home/$USER/.dotfiles/.bashrc' -> '/home/$USER/.bashrc'
```
now if you want to list the available dotfiles in your .dotfiles directory you type `lsrc` the results will be
```shell
/home/$USER/.bashrc:/home/ron/.dotfiles/bashrc
```
in your case, where the path before the `:` is the symlinks location and the path after `:` is the file in your .dotfiles directory.

The program that does all the magic of creates directories and symlinks your files is `rcup`, it installs and updates your dotfiles.
To install or update all the dotfiles you run `rcup` by itself, but you can specify a file, or directory as will, for example to only install your
.bashrc file you type
```
rcup -v .bashrc
```
the result of will be
```shell
'/home/$USER/.dotfiles/.bashrc' -> '/home/$USER/.bashrc'
```

To remove a symlink you use `rcdn` which will remove all symlinks managed by rcm, you can control this even further by specifying a file or list of files
or a directory using the `-d` flag. Notice that the file will not be removed from your .dotfiles directory and only the symlink will be removed. If you want to
restore the symlink you can do that with `rcup <filename>`. All the above applies to directories as will.

### backing up your .dotfiles folder

The best way to backup your dotfiles is to use git, and push them either to Github or Gitlab. To do this you'll first need to make a repository
on your Github or Gitlab named ".dotifles" and intialize it with a readme file.
Then cd yo your .dotfiles directory and do the follwoing
```shell
git init
git add . #stage all files inside current git directory
git commit -m "intial commit" #commit the staged changed and add a commit message
git add remote origin <link to your remote repository #add your remote repository
#then push everything to your remote repository with
git push origin master
```
now whenever you need to reinstall all your dotfiles all you have to do is clone the remote repository to your home folder then run `rcup` anywhere from the terminal.

### Advanced (cool) features

If you already have a dotfiles firectory and you want to use that instead of creating a new one, that is possible using ~/.rcrc file which is the config file of rcm and is sourced by all the other programs in the rcm tools suit. To change the default .dotfiles directory you add this line to your `.rcrc`
```shell
DOTFILES_DIRS="/path/to/your/dotfiles/directory"
```
but please note that your dotfiles directory must be organized correctly, meaning that the root of your .dotfiles directory is considered your $HOME directory. So if you are an bspwm user for example, you should have "config/bspwm/bspwmrc" inside of your .dotfiles directory so that rcup can symlink it to the correct location which is
`"$HOME/.config/bspwm/bspwmrc"`. Also not that .config is not dotted inside your .dotfiles directory, as rcup adds the dots when it makes the symlinks.

Another very amazing feature in rcm is tags. Quoting the official documentation

"If you share the dotfiles directory between people, you may end up with some irrelevant or even incorrect rc files.
For example, you may have a .zshrc while your other contributor has a .bashrc. This situation can be handled with tags."

To do that you have to create to directory inside your .dotfiles that starts with tag-, here you should create two, one called tag-bash and another called
tag-zsh, then move your .zshrc to your tag-zsh directory using mkrc
```
mkrc -t zsh .zshrc
#and your bash rc with
mkrc -t bash .bash
```
now whenever you are using your computer that runs zsh and you are installing or updating your dotfiles you must type
```
rcup -t zsh
```
and this will install your .zshrc to your system, and whenever on your other computer that runs bash, you replace `-t zsh` with `-t bash`.
You can set your default tags the `TAGS` variable inside your `.rcrc`.

### Undotted files and directories

If you have lets say, your Scripts directory in your home folder, and you add it to the list of dotfiles managed by rcm, the directory will be prefixed with a `.`making it hidden. If you want to exclude a file or directory from this behavior, you have to options. The first is to use the `-U` flag when adding the file or directory. For example
```
mkrc -U ~/Scripts
```
or you can add the file or directory to the `UNDOTTED` variable in your `.rcrc`.

### Conclusion

As you can see from the examples above, rcm is the way to go if you want to manage your dotfiles with style, it is powerful, easy to use and configure, and I hope more people will use it and contribute to it. rcm has many more features and capabilities that I didn't talk about in this post, if you want to learn more about rcm, head to it's official [documentation](https://thoughtbot.github.io/rcm/rcm.7.html) which is very detailed and clean. You can also add the `-h` flag to any of the four rcm programs to get a quick help on the terminal.

you can check my dotfiles [here](https://github.com/ronniedroid/.dotfiles)

