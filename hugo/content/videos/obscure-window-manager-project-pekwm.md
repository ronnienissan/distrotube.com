---
title: "Obscure Window Manager Project - PekWM"
image: images/thumbs/0115.jpg
date: Fri, 16 Feb 2018 01:54:35 +0000
author: Derek Taylor
tags: ["PekWM", ""]
---

#### VIDEO

{{< amazon src="Obscure+Window+Manager+Project+-+PekWM.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I take a look at number four of twelve "obscure" window managers that I'm taking a look at in this series of videos. PekWM is a floating window manager that features a right-click menu that supports dynamic menus--similar to Openbox. Lightweight and easy to configure.
