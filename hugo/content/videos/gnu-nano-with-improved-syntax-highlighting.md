---
title: "GNU Nano With Improved Syntax Highlighting"
image: images/thumbs/0377.jpg
date: Fri, 05 Apr 2019 02:44:42 +0000
author: Derek Taylor
tags: ["TUI Apps", "terminal"]
---

#### VIDEO

{{< amazon src="GNU+Nano+With+Improved+Syntax+Highlighting.mp4" >}}
&nbsp;

#### SHOW NOTES

Want improved syntax highlighting in GNU Nano? I will show you a dead simple way to get this working. 
