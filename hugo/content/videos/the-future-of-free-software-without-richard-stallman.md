---
title: "The Future Of Free Software Without Richard Stallman"
image: images/thumbs/0442.jpg
date: Tue, 17 Sep 2019 23:46:00 +0000
author: Derek Taylor
tags: ["Richard Stallman", ""]
---

#### VIDEO

{{< amazon src="The+Future+Of+Free+Software+Without+Richard+Stallman.mp4" >}}
&nbsp;

#### SHOW NOTES

Richard Stallman has resigned from both MIT and the Free Software Foundation. What now? What is the future of the free software movement?


REFERENCED:
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=lX3Y0vA7vBQ&amp;q=https%3A%2F%2Fwww.vice.com%2Fen_us%2Farticle%2F9ke3ke%2Ffamed-computer-scientist-richard-stallman-described-epstein-victims-as-entirely-willing&amp;redir_token=vdstIq_SHmP-UaFVYjNUELuM-qp8MTU3NzQ5MDM4M0AxNTc3NDAzOTgz" target="_blank" rel="nofollow noopener noreferrer">https://www.vice.com/en_us/article/9k...
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=lX3Y0vA7vBQ&amp;q=https%3A%2F%2Fblog.halon.org.uk%2F2019%2F09%2Fgnome-foundation-relationship-gnu-fsf%2F&amp;redir_token=vdstIq_SHmP-UaFVYjNUELuM-qp8MTU3NzQ5MDM4M0AxNTc3NDAzOTgz" target="_blank" rel="nofollow noopener noreferrer">https://blog.halon.org.uk/2019/09/gno...
+ ► <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?event=video_description&amp;v=lX3Y0vA7vBQ&amp;q=https%3A%2F%2Fwww.fsf.org%2F&amp;redir_token=vdstIq_SHmP-UaFVYjNUELuM-qp8MTU3NzQ5MDM4M0AxNTc3NDAzOTgz" target="_blank" rel="nofollow noopener noreferrer">https://www.fsf.org/
