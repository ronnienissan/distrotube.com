---
title: "Text To Speech On Linux With Festival"
image: images/thumbs/0543.jpg
date: 2020-02-13T12:22:40+06:00
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="Text+To+Speech+On+Linux+With+Festival.mp4" >}}
&nbsp;

#### SHOW NOTES


Festival is a general multi-lingual speech synthesizer (a text-to-speech program).  It will read documents saved on your system, or read highlighted text you have selected with your mouse or cursor.  Also, it can convert the document into an mp3 that you can then play with your audio player.

REFERENCED:
+  https://wiki.archlinux.org/index.php/Festival