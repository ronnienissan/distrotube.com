---
title: "Artix LXQt with Runit - Installation and First Look"
image: images/thumbs/0292.jpg
date: Fri, 05 Oct 2018 19:54:25 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Artix"]
---

#### VIDEO

{{< amazon src="Artix+LXQt+with+Runit.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm taking a quick look at Artix Linux with the LXQt desktop and the 
runit init system.  Artix is an Arch-based Linux distro that is focused 
on not using systemd.  

<a href="https://www.youtube.com/redirect?redir_token=LABNTOuOAvRXKaeN-q2z4Y88zsd8MTU1MzQ1NzM3NEAxNTUzMzcwOTc0&amp;v=bXkv73DDUdI&amp;q=https%3A%2F%2Fartixlinux.org%2F&amp;event=video_description" rel="noreferrer noopener" target="_blank">https://artixlinux.org/
