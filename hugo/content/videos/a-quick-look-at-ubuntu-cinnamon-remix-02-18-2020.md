---
title: "A Quick Look At Ubuntu Cinnamon Remix 20.04"
image: images/thumbs/0548.jpg
date: 2020-02-18T12:22:40+06:00
author: Derek Taylor
tags: ["Distro Reviews", "Ubuntu Cinnamon"]
---

#### VIDEO

{{< amazon src="A+Quick+Look+At+Ubuntu+Cinnamon+Remix+2004.mp4" >}}
&nbsp;

#### SHOW NOTES

I'm taking a quick look at Ubuntu Cinnamon Remix 20.04.  This is not an official flavor of Ubuntu and, being based off a yet-to-be-released Ubuntu 20.04, this is very much beta software.  But this young project is already shaping up to be a fine addition to the Ubuntu family.Today I will be broadcasting live and in color.  I will engage with the YouTube chat and address some questions and comments from the community.  We may dive into some Linux-y discussions, or we may discuss some other stuff.ay is yet another AUR helper (officially, Yet Another Yaourt) written in Go.  It is fast and it supports much of the same syntax that you already use in Pacman.  

REFERENCED:
+ https://sourceforge.net/projects/ubuntu-cinnamon-remix/