---
title: "Taking Into Account, Ep. 48 - LIVE"
image: images/thumbs/0432.jpg
date: Thu, 15 Aug 2019 23:21:00 +0000
author: Derek Taylor
tags: ["Taking Into Account", "Live Stream"]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+48+-+LIVE-P8-1kS-6fPM.mp4" >}}
&nbsp;

#### SHOW NOTES

This episode of Taking Into Account will be a special live edition.
+ Linux Journal shuts down...again. OSDisc also is going away.
+ Some EPYC new hardware! Building the default x86_64 kernel in just 16 seconds.
+ Linux 5.3 will address crackling audio on AMD PCs.
+ Why Canonical views the Snap ecosystem as a compelling distribution-agnostic solution.
+ A bit of FLOSS history. The FLOSS timeline for 1980 to 2000.
