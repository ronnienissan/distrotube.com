---
title: "My Favorite Linux Distro and Window Manager"
image: images/thumbs/0243.jpg
date: Sun, 15 Jul 2018 00:25:04 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="My+Favorite+Linux+Distro+and+Window+Manager.mp4" >}}
&nbsp;

#### SHOW NOTES

I get asked two questions more than anything else: (1) What is your favorite distro? (2) What is your favorite DE/WM? Here's my answer.
