---
title: "KDE Neon First Impression Install & Review"
image: images/thumbs/0134.jpg
date: Tue, 06 Mar 2018 21:23:11 +0000
author: Derek Taylor
tags: ["Distro Reviews", "KDE Neon", "KDE"]
---

#### VIDEO

{{< amazon src="KDE+Neon+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today I take a look at the very popular KDE Neon, an Ubuntu-based distro that sports the latest and greatest Plasma desktop. KDE Neon gets a ton of good press but, oddly enough, I've never looked at Neon myself. Should be fun! <a href="https://neon.kde.org/">https://neon.kde.org/</a>
