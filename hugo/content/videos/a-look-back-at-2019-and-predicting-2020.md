---
title: "A Look Back At 2019 And Predicting 2020"
image: images/thumbs/0504.jpg
date: 2019-12-27T12:22:40+06:00
author: Derek Taylor
tags: ["Live Stream", ""]
---

#### VIDEO

{{< amazon src="A+Look+Back+At++2019+And+Predicting+2020-zWIy9hkGF14.mp4" >}}
&nbsp;

#### SHOW NOTES

Let's take a look back at some of the big stories for Linux in 2019.  Let's also think about what 2020 has in store for us.  I will make my bold predictions for next year.
