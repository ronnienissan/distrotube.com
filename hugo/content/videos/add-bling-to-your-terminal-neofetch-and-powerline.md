---
title: "Add Bling to Your Terminal With Neofetch and Powerline Shell"
image: images/thumbs/0223.jpg
date: Fri, 01 Jun 2018 23:48:21 +0000
author: Derek Taylor
tags: ["terminal", "commmand line"]
---

#### VIDEO

{{< amazon src="Add+Bling+to+Your+Terminal+With+Neofetch+and+Powerline+Shell.mp4" >}}
&nbsp;

#### SHOW NOTES

I going to install a couple of programs to help make our shell a little bit sexier. 

https://github.com/dylanaraps/neofetch 

https://github.com/b-ryan/powerline-shell
