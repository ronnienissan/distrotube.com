---
title: "Quit Installing Arch on Family and Friend's Computers"
image: images/thumbs/0145.jpg
date: Fri, 16 Mar 2018 21:37:27 +0000
author: Derek Taylor
tags: ["Arch Linux", ""]
---

#### VIDEO

{{< amazon src="Quit+Installing+Arch+on+Family+and+Friends+Computers.mp4" >}}  
&nbsp;

#### SHOW NOTES

A short rant about a couple of the dumb things I see otherwise smart people doing in regards to installing Linux on computers of family members and friends. 
