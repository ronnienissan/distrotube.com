---
title: "Nitrux Linux 1.0.8 First Impression Install & Review"
image: images/thumbs/0113.jpg
date: Tue, 13 Feb 2018 01:52:20 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Nitrux Linux", "KDE"]
---

#### VIDEO

{{< amazon src="Nitrux+Linux+1.0.8+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at Nitrux Linux--an Ubuntu-based Linux distro that focuses on AppImages and sports the Nomad desktop which sits on top of KDE Plasma 5.  
