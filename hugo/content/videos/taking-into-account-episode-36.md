---
title: "Taking Into Account, Ep. 36 - Linus Interview, Linux Gaming, VMWare, SPURV, Ubuntu, Fedora"
image: images/thumbs/0376.jpg
date: Wed, 03 Apr 2019 02:39:45 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+36.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=AJNxjjPQKKM&amp;t=45s">0:45 Linus Torvalds sat down for an interview.  Fears that he can't say "silly stupid crap" anymore. 

<a href="https://www.youtube.com/watch?v=AJNxjjPQKKM&amp;t=690s">11:30 Linux gaming won't find a mainstream audience until three things happen. 

<a href="https://www.youtube.com/watch?v=AJNxjjPQKKM&amp;t=1166s">19:26 For over 10 years, VMware was accused of illegally using Linux code.  Lawsuit now dropped. 

<a href="https://www.youtube.com/watch?v=AJNxjjPQKKM&amp;t=1458s">24:18 Wouldn’t it be great if you could run your favorite Android apps in Linux?  Announcing SPURV. 

<a href="https://www.youtube.com/watch?v=AJNxjjPQKKM&amp;t=1675s">27:55 The next big Ubuntu and Fedora releases are getting that much closer.  Betas are out! 

<a href="https://www.youtube.com/watch?v=AJNxjjPQKKM&amp;t=1893s">31:33 Why was this episode of Taking Into Account released a day early? 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AJNxjjPQKKM&amp;q=https%3A%2F%2Fwww.linuxjournal.com%2Fcontent%2F25-years-later-interview-linus-torvalds&amp;redir_token=DQMA19J7BBAI_PUoUOTNIMgszT58MTU1NzE5NjgyOEAxNTU3MTEwNDI4" target="_blank">https://www.linuxjournal.com/content/...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AJNxjjPQKKM&amp;q=https%3A%2F%2Fwww.forbes.com%2Fsites%2Fjasonevangelho%2F2019%2F04%2F03%2Flinux-gaming-steam-fortnite-windows-opinion%2F%2343a19c2b8fce&amp;redir_token=DQMA19J7BBAI_PUoUOTNIMgszT58MTU1NzE5NjgyOEAxNTU3MTEwNDI4" target="_blank">https://www.forbes.com/sites/jasoneva...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AJNxjjPQKKM&amp;q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Flinux-developer-abandons-vmware-lawsuit%2F&amp;redir_token=DQMA19J7BBAI_PUoUOTNIMgszT58MTU1NzE5NjgyOEAxNTU3MTEwNDI4" target="_blank">https://www.zdnet.com/article/linux-d...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AJNxjjPQKKM&amp;q=https%3A%2F%2Fsfconservancy.org%2F&amp;redir_token=DQMA19J7BBAI_PUoUOTNIMgszT58MTU1NzE5NjgyOEAxNTU3MTEwNDI4" target="_blank">https://sfconservancy.org/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AJNxjjPQKKM&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F04%2Fnew-project-brings-android-apps-to-the-linux-desktop&amp;redir_token=DQMA19J7BBAI_PUoUOTNIMgszT58MTU1NzE5NjgyOEAxNTU3MTEwNDI4" target="_blank">https://www.omgubuntu.co.uk/2019/04/n...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AJNxjjPQKKM&amp;q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F03%2Fdownload-ubuntu-19-04-beta-iso&amp;redir_token=DQMA19J7BBAI_PUoUOTNIMgszT58MTU1NzE5NjgyOEAxNTU3MTEwNDI4" target="_blank">https://www.omgubuntu.co.uk/2019/03/d...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?event=video_description&amp;v=AJNxjjPQKKM&amp;q=https%3A%2F%2Ffedoramagazine.org%2Fannouncing-the-release-of-fedora-30-beta%2F&amp;redir_token=DQMA19J7BBAI_PUoUOTNIMgszT58MTU1NzE5NjgyOEAxNTU3MTEwNDI4" target="_blank">https://fedoramagazine.org/announcing...
