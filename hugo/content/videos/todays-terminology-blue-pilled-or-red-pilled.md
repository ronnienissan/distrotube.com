---
title: "Today's Terminology - Blue Pilled or Red Pilled"
image: images/thumbs/0452.jpg
date: Mon, 28 Oct 2019 00:02:00 +0000
author: Derek Taylor
tags: ["memes", ""]
---

#### VIDEO

{{< amazon src="Todays+Terminology+Blue+Pilled+or+Red+Pilled.mp4" >}}
&nbsp;

#### SHOW NOTES

In this video, I'm going to discuss the changing of our speech patterns and the new terms that everyone is using now. Are you based and red pilled? Or are you cringe and blue pilled?

#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=KgLrhcghTt49LZR-z8sjfnDlzJB8MTU3NzQ5MTMzNkAxNTc3NDA0OTM2&amp;event=video_description&amp;v=V7SAd4AUvXo&amp;q=https%3A%2F%2Fwww.urbandictionary.com%2F" target="_blank" rel="nofollow noopener noreferrer">https://www.urbandictionary.com/
