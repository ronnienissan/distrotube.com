---
title: "Microsoft Edge Coming To Linux? Google Essentially Owns The Internet"
image: images/thumbs/0374.jpg
date: Sat, 30 Mar 2019 16:34:56 +0000
author: Derek Taylor
tags: ["Microsoft", ""]
---

#### VIDEO

{{< amazon src="Microsoft+Edge+Coming+To+Linux+Google+Essentially+Owns+The+Internet.mp4" >}}
&nbsp;

#### SHOW NOTES

Is Microsoft Edge coming to Linux?  Plus, now that Edge has changed web  engines, Google's domination of the Internet is nearly complete.  

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.ghacks.net%2F2019%2F03%2F29%2Fchromium-based-edge-linux-support-and-ie-integration%2F&amp;redir_token=jC7hkuIv0F6hjGjfIbCuWtdHGSF8MTU1NDA0OTg5N0AxNTUzOTYzNDk3&amp;event=video_description&amp;v=4qag2Z_Zre4" target="_blank">https://www.ghacks.net/2019/03/29/chr...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.mozilla.org%2F&amp;redir_token=jC7hkuIv0F6hjGjfIbCuWtdHGSF8MTU1NDA0OTg5N0AxNTUzOTYzNDk3&amp;event=video_description&amp;v=4qag2Z_Zre4" target="_blank">https://www.mozilla.org/
