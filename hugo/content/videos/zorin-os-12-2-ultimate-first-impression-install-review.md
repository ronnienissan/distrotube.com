---
title: "Zorin OS 12.2 Ultimate First Impression Install & Review"
image: images/thumbs/0104.jpg
date: Sat, 03 Feb 2018 01:39:06 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Zorin OS"]
---

#### VIDEO

{{< amazon src="Zorin+OS+12+2+Ultimate+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

Today, I take a look at Zorin OS 12.2 Ultimate which is an Ubuntu-based Linux distribution that is aimed toward making Windows and Mac users feel at home on Linux. The Ultimate edition of Zorin costs € 19 (about $ 24 US for me). Zorin also offers a free version.<a href="https://zorinos.com/"> https://zorinos.com/</a>
