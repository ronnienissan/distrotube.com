---
title: "Elementary OS Is The Distro We Should Be Showing Potential New Linux Users"
image: images/thumbs/0286.jpg
date: Fri, 21 Sep 2018 19:38:26 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Elementary OS"]
---

#### VIDEO

{{< amazon src="Elementary+OS+Is+The+Distro+We+Should+Be+Showing+Potential+New+Linux+Users.mp4" >}}
&nbsp;

#### SHOW NOTES

I take the new Beta 2 release of the upcoming Elementary OS 5.0 "Juno"  for a spin.  But really this video is about Elementary OS as an  operating system in general, and how I think THIS is the distro we  should be showing to potential new-to-Linux users coming over from  Windows/Mac. 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=SaLWmWAA2L0&amp;event=video_description&amp;q=https%3A%2F%2Felementary.io%2F&amp;redir_token=dwVZw6klDTcaBddDsDUP3LQMxfh8MTU1MzQ1NjI5MEAxNTUzMzY5ODkw" target="_blank">https://elementary.io/

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?v=SaLWmWAA2L0&amp;event=video_description&amp;q=https%3A%2F%2Fmedium.com%2Felementaryos%2Fdeveloper-preview-juno-beta-2-is-out-418f5037e9bb&amp;redir_token=dwVZw6klDTcaBddDsDUP3LQMxfh8MTU1MzQ1NjI5MEAxNTUzMzY5ODkw" target="_blank">https://medium.com/elementaryos/devel...
