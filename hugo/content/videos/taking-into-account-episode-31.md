---
title: "Taking Into Account, Ep. 31 - Ubuntu Drops APT, Ransomware, Redis, Social Media Crime, Icons"
image: images/thumbs/0356.jpg
date: Thu, 28 Feb 2019 23:01:37 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account+Ep+31.mp4" >}}
&nbsp;

#### SHOW NOTES

On this edition of Taking Into Account: 

<a href="https://www.youtube.com/watch?v=O682IhXZKB8&amp;t=46s">0:46 No, Ubuntu is NOT Replacing Apt with Snap. 

<a href="https://www.youtube.com/watch?v=O682IhXZKB8&amp;t=480s">8:00 B0r0nt0K ransomware wants $75,000 ransom, infects Linux servers. 

<a href="https://www.youtube.com/watch?v=O682IhXZKB8&amp;t=768s">12:48 Redis Labs drops Commons Clause for a new license.  

<a href="https://www.youtube.com/watch?v=O682IhXZKB8&amp;t=1166s">19:26 Cybercriminals earn over $3 billion a year from social platforms. 

<a href="https://www.youtube.com/watch?v=O682IhXZKB8&amp;t=1561s">26:01 Ubuntu icon set addresses its biggest complaint.   

<a href="https://www.youtube.com/watch?v=O682IhXZKB8&amp;t=1794s">29:54 I read an email question from a viewer. 

📰 REFERENCED: 

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fitsfoss.com%2Fubuntu-snap-replaces-apt-blueprint%2F&amp;v=O682IhXZKB8&amp;redir_token=oCJsCbuMjRnmm2g5pjzM75I6iJ58MTU1MzY0MTMxMkAxNTUzNTU0OTEy&amp;event=video_description" target="_blank">https://itsfoss.com/ubuntu-snap-repla...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.bleepingcomputer.com%2Fnews%2Fsecurity%2Fb0r0nt0k-ransomware-wants-75-000-ransom-infects-linux-servers%2F&amp;v=O682IhXZKB8&amp;redir_token=oCJsCbuMjRnmm2g5pjzM75I6iJ58MTU1MzY0MTMxMkAxNTUzNTU0OTEy&amp;event=video_description" target="_blank">https://www.bleepingcomputer.com/news...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.zdnet.com%2Farticle%2Fredis-labs-drops-commons-clause-for-a-new-license%2F&amp;v=O682IhXZKB8&amp;redir_token=oCJsCbuMjRnmm2g5pjzM75I6iJ58MTU1MzY0MTMxMkAxNTUzNTU0OTEy&amp;event=video_description" target="_blank">https://www.zdnet.com/article/redis-l...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fbetanews.com%2F2019%2F02%2F26%2Fcybercriminals-3bn-social-platform-earnings%2F&amp;v=O682IhXZKB8&amp;redir_token=oCJsCbuMjRnmm2g5pjzM75I6iJ58MTU1MzY0MTMxMkAxNTUzNTU0OTEy&amp;event=video_description" target="_blank">https://betanews.com/2019/02/26/cyber...

<a rel="noreferrer noopener" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.omgubuntu.co.uk%2F2019%2F02%2Fubuntu-yaru-icons-non-uniform&amp;v=O682IhXZKB8&amp;redir_token=oCJsCbuMjRnmm2g5pjzM75I6iJ58MTU1MzY0MTMxMkAxNTUzNTU0OTEy&amp;event=video_description" target="_blank">https://www.omgubuntu.co.uk/2019/02/u...
