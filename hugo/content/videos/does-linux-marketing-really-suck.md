---
title: "Does Linux Marketing Really Suck?"
image: images/thumbs/0458.jpg
date: Tue, 05 Nov 2019 00:10:00 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Does+Linux+Marketing+Suck.mp4" >}}
&nbsp;

#### SHOW NOTES

In the last few months, I'm seeing a lot more writers, bloggers, podcasters, and YouTubers call for "Linux" to do a better job at marketing itself. Do these people know what they are really asking?
