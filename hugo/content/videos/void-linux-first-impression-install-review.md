---
title: "Void Linux First Impression Install & Review"
image: images/thumbs/0077.jpg
date: Mon, 01 Jan 2018 00:49:07 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Void Linux"]
---

#### VIDEO

{{< amazon src="Void+Linux+First+Impression+Install+%26+Review.mp4" >}}I start the New Year by looking at an independent distro called Void Linux. Void is interesting in that it a rolling release, includes its own init system (runit) and uses it own package manager (xbps). <a href="https://www.voidlinux.eu/">https://www.voidlinux.eu/</a>
