---
title: "Installing and Configuring Openbox in Arch Linux"
image: images/thumbs/0227.jpg
date: Fri, 08 Jun 2018 23:58:08 +0000
author: Derek Taylor
tags: ["Arch Linux", "Openbox"]
---

#### VIDEO

{{< amazon src="Installing+and+Configuring+Openbox+in+Arch+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I will continue where I left off from my previous Arch Linux installation ( https://www.youtube.com/watch?v=EN9yo4dJ-O4 ).
