---
title: "ArchLabs 2018.12.17 (bspwm) - Installation and First Look"
image: images/thumbs/0320.jpg
date: Fri, 21 Dec 2018 19:36:35 +0000
author: Derek Taylor
tags: ["Distro Reviews", "ArchLabs", "bspwm"]
---

#### VIDEO

{{< amazon src="ArchLabs+Linux+20181217+Installation+and+First+Look.mp4" >}}
&nbsp;

#### SHOW NOTES

Today, I'm taking a look at the recently released ArchLabs 2018.12.17. It's a radical departure from previous versions. There is no live environment, it launches straight into an installer, and you now have several choices for DE/WM. During this installation I choose to install bspwm--a nice little tiling window manager.
