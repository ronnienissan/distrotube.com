---
title: "Pass - The Standard Unix Password Manager"
image: images/thumbs/0405.jpg
date: Tue, 28 May 2019 03:22:27 +0000
author: Derek Taylor
tags: ["Security", "command line"]
---

#### VIDEO

{{< amazon src="Pass+The+Standard+Unix+Password+Manager.mp4" >}}
&nbsp;

#### SHOW NOTES

Looking for a small yet powerful password manager that has plugins for most web browsers and builtin dmenu support? Look no further! And this password manager adheres to the Unix Philosophy.


&nbsp;
#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?redir_token=oYU5kEbuq60sJH0_CL9_B-oyyKh8MTU2NjUzMDYxMEAxNTY2NDQ0MjEw&amp;event=video_description&amp;v=hlRQTj1D9LA&amp;q=https%3A%2F%2Fwww.passwordstore.org%2F" target="_blank" rel="nofollow noopener noreferrer">https://www.passwordstore.org/
