---
title: "Playing With Emacs, Xmonad and Xmobar - DT Live"
image: images/thumbs/0500.jpg
date: 2019-10-08T12:22:40+06:00
author: Derek Taylor
tags: ["Live Stream", "Emacs", "xmonad"]
---

#### VIDEO

{{< amazon src="Playing+With+Emacs%2C+Xmonad+and+Xmobar+-+DT+Live-fnoEZ0VVwNA.mp4" >}}
&nbsp;

#### SHOW NOTES

Just playing around with some configs.  I'm back to old stomping ground--xmonad!  Tweaking xmobar a bit.  Plus looking a bit more at emacs.
