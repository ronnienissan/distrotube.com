---
title: "Checking Out Ben's Custom Manjaro Spin and Presentarms PCLinuxOS Trinity - DT LIVE"
image: images/thumbs/0219.jpg
date: Sun, 27 May 2018 23:40:11 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Trinity", "Live Stream"]
---

#### VIDEO

{{< amazon src="Checking+Out+Bens+Custom+Manjaro+Spin+and+Presentarms+PCLinuxOS+Trinity+-+DT+LIVE.mp4" >}}
&nbsp;

#### SHOW NOTES

Tonight, I'm going to take a look Ben Fitzpatrick's custom Manjaro spin. Will also take a look at presentarms' custom PCLinuxOS Trinity. 

Download Ben's distro: https://drive.google.com/file/d/1DXa44Ztq8UXX-ZQisfJT07MK_GZgQ85K/view

Download PCLinuxOS Trinity: http://trinity.mypclinuxos.com/
