---
title: "A Tiling Desktop Environment? Introducing The Pop Shell!"
image: images/thumbs/0575.jpg
date: 2020-03-21T12:22:40+06:00
author: Derek Taylor
tags: ["Tiling Window Managers", "GNOME"]
---

#### VIDEO

{{< amazon src="A+Tiling+Desktop+Environment+Introducing+The+Pop+Shell.mp4" >}}
&nbsp;

#### SHOW NOTES

Pop Shell is a keyboard-driven layer for GNOME Shell which allows for quick and sensible navigation and management of windows. The core feature of Pop Shell is the addition of advanced tiling window management.  This project is currently in beta; developed for inclusion in Pop!_OS 20.04 at release.

REFERENCED:
+ https://github.com/pop-os/shell