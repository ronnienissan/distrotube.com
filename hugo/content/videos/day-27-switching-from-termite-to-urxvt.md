---
title: "Day 27 - Switching from Termite to URXVT"
image: images/thumbs/0199.jpg
date: Mon, 07 May 2018 23:03:55 +0000
author: Derek Taylor
tags: ["Tiling Window Managers", "terminal"]
---

#### VIDEO

{{< amazon src="Day+27+-+Switching+from+Termite+to+URXVT.mp4" >}}
&nbsp;

#### SHOW NOTES

Day 27 of my 30 challenge. I'm living in a tiling window manager for 30 days and only using terminal-based applications where possible. So 27 days in...and I've decided to switch away from the terminal emulator I've been using for the last couple of years. 
