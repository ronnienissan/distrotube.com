---
title: "Chat With Patrons (March 24, 2019)"
image: images/thumbs/0485.jpg
date: Sun, 24 Mar 2019 19:02:44 +0000
author: Derek Taylor
tags: ["Chat With Patrons", ""]
---

#### VIDEO

{{< amazon src="Chat+With+Patrons+(March+24%2C+2019)-is5SH9a-RFM.mp4" >}}
&nbsp;

#### SHOW NOTES

Today's live stream will be a special event for my Patreon supporters. This will be a Zoom video chat just for my patrons. The link to the video chat call will be posted on my Patreon page a few minutes prior to the stream.
