---
title: "DT Game Night - Ballistic Overkill"
image: images/thumbs/0497.jpg
date: 2019-10-30T12:22:40+06:00
author: Derek Taylor
tags: ["Live Stream", "Gaming"]
---

#### VIDEO

{{< amazon src="DT+Game+Night+-+Ballistic+Overkill-BSeH3_E57yM.mp4" >}}
&nbsp;

#### SHOW NOTES

Let's play some Ballistic Overkill, a fast paced first person shooter available on Steam...and for Linux!  Want to chat with me in game and on stream? 
