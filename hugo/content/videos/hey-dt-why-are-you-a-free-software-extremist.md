---
title: "Hey, DT. Why Are You A Free Software Extremist? (Plus Other Questions Answered)"
image: images/thumbs/0599.jpg
date: 2020-04-17T12:23:40+06:00
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Hey%2C+DT+Why+Are+You+A+Free+Software+Extremist.mp4" >}}
&nbsp;

#### SHOW NOTES

In this lengthy rant video, I address a few questions that I've been receiving from viewers.  I discuss my current job situation, the camera angle on my videos, why I window manager hop, and discuss some channel and website housekeeping.