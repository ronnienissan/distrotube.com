---
title: "Netrunner Rolling 2018.01 First Impression Install & Review"
image: images/thumbs/0110.jpg
date: Fri, 09 Feb 2018 01:47:57 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Netrunner", "KDE"]
---

#### VIDEO

{{< amazon src="Netrunner+Rolling+2018.01+First+Impression+Install+%26+Review.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I take a quick look at Netrunner Rolling. It is a Manjaro-based Linux distribution that features the KDE desktop environment and a nice suite of programs installed out-of-the-box. 
