---
title: "Cava, aafire and unixporn"
image: images/thumbs/0124.jpg
date: Sun, 25 Feb 2018 21:09:01 +0000
author: Derek Taylor
tags: ["Terminal", "Command Line"]
---

#### VIDEO

{{< amazon src="Cava%2C+aafire+and+unixporn.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video, I briefly discuss a couple of terminal-based applications (cava and aafire) and also talk about the unixporn subreddit. For those that want to see my post on unixporn, check it out here: <a href="https://www.reddit.com/r/unixporn/comments/805ta9/openbox_manjaro_and_my_most_used_applications/">https://www.reddit.com/r/unixporn/comments/805ta9/openbox_manjaro_and_my_most_used_applications/</a>
