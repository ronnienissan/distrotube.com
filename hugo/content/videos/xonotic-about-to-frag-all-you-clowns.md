---
title: "Xonotic - About To Frag All You Clowns!"
image: images/thumbs/0288.jpg
date: Sat, 22 Sep 2018 19:42:51 +0000
author: Derek Taylor
tags: ["Gaming", "Live Stream"]
---

#### VIDEO

{{< amazon src="Xonotic+-+About+To+Frag+All+You+Clowns!.mp4" >}}
&nbsp;

#### SHOW NOTES

About to play a little Xonotic, a free and open source first person 
shooter that runs great on Linux!  Don't have Xonotic?  Installing it as
 a snap package is probably the best way.  You can also download it off 
of the Xonotic website.

<a href="https://www.youtube.com/redirect?v=rzfRnopaXhc&amp;event=video_description&amp;redir_token=mAQU6_xgQ2qD2wn0nZFDqOxXeTF8MTU1MzQ1NjU4NkAxNTUzMzcwMTg2&amp;q=https%3A%2F%2Fwww.xonotic.org%2F" rel="noreferrer noopener" target="_blank">https://www.xonotic.org/
