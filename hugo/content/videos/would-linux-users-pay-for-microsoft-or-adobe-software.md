---
title: "Would Linux Users Pay For Microsoft Or Adobe Software?"
image: images/thumbs/0415.jpg
date: Sat, 22 Jun 2019 05:29:00 +0000
author: Derek Taylor
tags: ["Microsoft", "Proprietary Software"]
---

#### VIDEO

{{< amazon src="Would+Linux+Users+Pay+For+Microsoft+Or+Adobe+Software.mp4" >}}
&nbsp;

#### SHOW NOTES

Would Linux users pay for Microsoft's Office suite or Adobe's Creative Suite? Is Linux a viable platform for proprietary software? And is there a substantial portion of our community that would prefer to never see these proprietary solutions become available on Linux?

DONATE TO THESE PROJECTS:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=X65liTjApso&amp;event=video_description&amp;redir_token=opDAeVsLZyJOIfIxeZLZMLdNjwd8MTU3NTY5NjY0OUAxNTc1NjEwMjQ5&amp;q=https%3A%2F%2Fwww.gimp.org%2Fdonating%2F" target="_blank" rel="nofollow noopener noreferrer">https://www.gimp.org/donating/
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=X65liTjApso&amp;event=video_description&amp;redir_token=opDAeVsLZyJOIfIxeZLZMLdNjwd8MTU3NTY5NjY0OUAxNTc1NjEwMjQ5&amp;q=https%3A%2F%2Finkscape.org%2Fsupport-us%2Fdonate%2F" target="_blank" rel="nofollow noopener noreferrer">https://inkscape.org/support-us/donate/
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?v=X65liTjApso&amp;event=video_description&amp;redir_token=opDAeVsLZyJOIfIxeZLZMLdNjwd8MTU3NTY5NjY0OUAxNTc1NjEwMjQ5&amp;q=https%3A%2F%2Fwww.libreoffice.org%2Fdonate%2F" target="_blank" rel="nofollow noopener noreferrer">https://www.libreoffice.org/donate/
