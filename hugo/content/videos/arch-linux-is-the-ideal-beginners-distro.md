---
title: "Arch Linux Is The Ideal Beginner's Distro"
image: images/thumbs/0454.jpg
date: Fri, 01 Nov 2019 00:05:00 +0000
author: Derek Taylor
tags: ["Arch Linux", ""]
---

#### VIDEO

{{< amazon src="Arch+Linux+Is+The+Ideal+Beginners+Distro.mp4" >}}
&nbsp;

#### SHOW NOTES

I've gone back and forth on this for awhile, but I've come to the conclusion that not only is Arch easy enough for the beginner to install, Arch is the ideal beginner's distro.

#### REFERENCED:
+ <a class="yt-simple-endpoint style-scope yt-formatted-string" spellcheck="false" href="https://www.youtube.com/redirect?q=https%3A%2F%2Fwww.archlinux.org%2F&amp;event=video_description&amp;v=01IX7jRgft8&amp;redir_token=0qBF37xdYvygl4Tgzs0axROLPF58MTU3NzQ5MTU0MEAxNTc3NDA1MTQw" target="_blank" rel="nofollow noopener noreferrer">https://www.archlinux.org/
