---
title: "The Command Line Does It Better Than The GUI"
image: images/thumbs/0228.jpg
date: Sat, 09 Jun 2018 00:00:07 +0000
author: Derek Taylor
tags: ["terminal", "command line"]
---

#### VIDEO

{{< amazon src="The+Command+Line+Does+It+Better+Than+The+GUI.mp4" >}}
&nbsp;

#### SHOW NOTES

We fear what we don't understand. The command line is no exception. But with knowledge comes great power!
