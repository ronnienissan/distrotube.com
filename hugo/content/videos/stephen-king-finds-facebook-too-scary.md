---
title: "Stephen King Finds Facebook Too Scary"
image: images/thumbs/0535.jpg
date: 2020-02-04T12:22:40+06:00
author: Derek Taylor
tags: ["Social Media", "Facebook", "Mastodon"]
---

#### VIDEO

{{< amazon src="Stephen+King+Finds+Facebook+Too+Scary.mp4" >}}
&nbsp;

#### SHOW NOTES

Stephen King recently announced that he is leaving Facebook over concerns about the political advertisements that Facebook serves.  Facebook delivers highly targetted ads to its users, including political ads, and Facebook does not care whether those ads are factual or contain false information.   

REFERENCED:
+  https://joinmastodon.org/ - Join Mastodon!
+  https://www.fsf.org/ -  Free Software Foundation
+  https://www.eff.org/ - Electronic Frontier Foundation