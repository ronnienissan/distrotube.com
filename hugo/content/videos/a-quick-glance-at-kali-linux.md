---
title: "A Quick Glance at Kali Linux"
image: images/thumbs/0119.jpg
date: Wed, 21 Feb 2018 20:58:18 +0000
author: Derek Taylor
tags: ["Distro Reviews", "Kali Linux"]
---

#### VIDEO

{{< amazon src="A+Quick+Glance+at+Kali+Linux.mp4" >}}
&nbsp;

#### SHOW NOTES

I take a quick look at the installation procedure for Kali Linux and go through some of the software installed by default. NOTE: I'm not a security, forensics or penetration expert. This is a just cursory glance at Kali.    https://www.kali.org/
