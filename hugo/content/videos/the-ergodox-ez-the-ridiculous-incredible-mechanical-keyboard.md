---
title: "The ErgoDox EZ - The Ridiculous, Incredible Mechanical Keyboard"
image: images/thumbs/0396.jpg
date: Sat, 18 May 2019 14:38:29 +0000
author: Derek Taylor
tags: ["ErgoDox EZ", ""]
---

#### VIDEO

{{< amazon src="The+ErgoDox+EZ+The+Ridiculous+Incredible+Mechanical+Keyboard.mp4" >}}
&nbsp;

#### SHOW NOTES

So I bought an ErgoDox EZ, an ergonomic programmable mechanical keyboard. It's ridiculous and awesome, rolled into one package.

&nbsp;
#### REFERENCED:
+ https://ergodox-ez.com/
+ https://www.pjrc.com/teensy/loader_linux.html
+ https://github.com/qmk/qmk_firmware/blob/master/layouts/community/ergodox/qwerty_code_friendly/readme.md
