---
title: "Taking Into Account, Ep. 3 - LibreOffice, Winepak, Desktop Linux, i3, Snapchat"
image: images/thumbs/0255.jpg
date: Thu, 09 Aug 2018 22:34:02 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+3+-+LibreOffice%2C+Winepak%2C+Desktop+Linux%2C+i3%2C+Snapchat.mp4" >}}
&nbsp;

#### SHOW NOTES

+ 0:32 LibreOffice 6.1 released with major changes 
+ 7:22 Winepak makes installing Windows apps on Linux easier 
+ 13:22 The failure of "desktop Linux has been a good thing? 
+ 22:03 Five reasons how i3 makes Linux better 
+ 30:36 Snapchat's source code leaked and posted at GitHub 
+ 34:20 I read a question from one of the viewers
