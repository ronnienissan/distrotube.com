---
title: "YouTube and Patreon - Mistakes Made and Lessons Learned"
image: images/thumbs/0282.jpg
date: Sat, 08 Sep 2018 19:04:44 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="YouTube+and+Patreon+-+Mistakes+Made+and+Lessons+Learned.mp4" >}}
&nbsp;

#### SHOW NOTES

I ramble on for quite some time in this video, mostly about some of my mistakes over the past eleven months regarding this YouTube channel, staying engaged with the community, and how I've mismanaged Patreon.
