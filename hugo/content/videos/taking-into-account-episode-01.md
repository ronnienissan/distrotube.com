---
title: "Taking Into Account, Ep. 1 - A New Show, Slackware, Deepin Store, Windows Winning, Ubuntu 18.04.1"
image: images/thumbs/0250.jpg
date: Thu, 26 Jul 2018 22:13:26 +0000
author: Derek Taylor
tags: ["Taking Into Account", ""]
---

#### VIDEO

{{< amazon src="Taking+Into+Account%2C+Ep.+1+-+A+New+Show%2C+Slackware%2C+Deepin+Store%2C+Windows+Winning%2C+Ubuntu+18.04.1.mp4" >}}
&nbsp;

#### SHOW NOTES

This weeks topics include:
+ 0:32 About this new show 
+ 5:10 Slackware dev needs funds 
+ 9:30 Deepin Store v5.0 released without "spyware" 
+ 14:20 Lower Saxony switches to Windows 
+ 19:30 Ubuntu 18.04.01 is released
