---
title: "Cmus - Terminal-Based Music Player - Lightweight and Super-Fast"
image: images/thumbs/0027.jpg
date: Thu, 02 Nov 2017 02:31:38 +0000
author: Derek Taylor
tags: ["TUI Apps", "cmus"]
---

#### VIDEO

{{< amazon src="Cmus+-+Terminal-Based+Music+Player+-+Lightweight+and+Super-Fast.mp4" >}}  
&nbsp;

#### SHOW NOTES

In this video I give a brief overview of cmus (the C Music Player). It is a console-based music player that is fast and lightweight and runs on any Unix-like operating system. Perfect for those users who spend a lot of time in the terminal or those with machines that are low on resources.
