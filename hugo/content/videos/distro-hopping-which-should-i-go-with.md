---
title: "Distro-Hopping. Which Distro Should I Go With? Help Me Decide!"
image: images/thumbs/0051.jpg
date: Wed, 06 Dec 2017 14:42:25 +0000
author: Derek Taylor
tags: ["", ""]
---

#### VIDEO

{{< amazon src="Distro-Hopping.+Which+Distro+Should+I+Go+With+Help+Me+Decide!.mp4" >}}  
&nbsp;

#### SHOW NOTES

I've been thinking about switching out my main machine from the Ubuntu LTS to something different. I just have that itch (the distro-hopping itch) and I want to scratch it. But which distro should I install? I have some ideas but I would love to get input from you guys!
